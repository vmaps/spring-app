# Spring Boot 应用程序

## 项目简介
这是一个基于 Spring Boot 的应用程序，使用 MyBatis-Plus 进行数据访问，支持 JWT 认证和 Druid 数据源管理。

## 技术栈
- Java 1.8
- Spring Boot 2.2.6
- MySQL
- MyBatis-Plus
- Apache Shiro
- Druid 数据源
- Lombok

## 项目结构
```
src
├── main
│ ├── java
│ │ └── com
│ │ └── wangsong
│ │ ├── CodeGenerator.java
│ │ ├── Test.java
│ │ └── common
│ │ └── model
│ │ └── CodeEnum.java
│ │ └── system
│ │ ├── controller
│ │ │ └── ResourcesController.java
│ │ └── entity
│ │ └── service
│ └── resources
│ ├── application.properties
│ └── mapper
│ └── system
│ └── ResourcesMapper.xml
└── test
```
## 配置
在 `src/main/resources/application.properties` 中配置数据库连接信息：

properties
server.port=8081
spring.datasource.url=jdbc:mysql://localhost:3306/springboot-app?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8
spring.datasource.username=root
spring.datasource.password=root
spring.datasource.driver-class-name=com.mysql.jdbc.Driver

## 运行项目
1. 确保 MySQL 数据库已启动，并创建名为 `springboot-app` 的数据库。
2. 使用 Maven 构建项目：
   ```bash
   mvn clean install
   ```
3. 运行主类 `CodeGenerator` 以生成代码。
4. 启动应用程序：
   ```bash
   mvn spring-boot:run
   ```

## 许可证
本项目遵循 Apache License 2.0。有关详细信息，请参阅 `LICENSE` 文件。
